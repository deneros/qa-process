# -*- coding: utf-8 -*
{
    'name': 'Quality Assurance Process',
    'version': '1.0',
    'category': 'QA Tool',
    'description': 'Denero Team Quality Assurance Process',
    'author': 'Denero Team',
    'website': 'http://www.deneroteam.com',
    'depends': [
        'project',
    ],
    'data': [
        # Views
        'views/qa_view.xml',
        # Wizard
        'wizard/wiz_project_view.xml',
        'wizard/wiz_project_task_view.xml',
        # Menus
        'menus/qa_menu.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'images': [],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

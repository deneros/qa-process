from openerp import models, fields, api


class qa_case(models.Model):
    _name = 'quality.assurance.case'
    _description = 'QA Case'

    # Columns
    code = fields.Char('Code', size=32)
    name = fields.Char('Name', size=128)
    category = fields.Many2one(
        'quality.assurance.case.category', string="Category"
    )
    steps = fields.One2many(
        'quality.assurance.case.step',
        'qa_case', string="Cases Steps"
    )


class qa_case_category(models.Model):
    _name = 'quality.assurance.case.category'
    _description = 'QA Case Category'

    # Columns
    name = fields.Char('Name', size=128)
    parent = fields.Many2one(
        'quality.assurance.case.category', string='Parent',
        select=True, ondelete='cascade'
    )
    child = fields.One2many(
        'quality.assurance.case.category', 'parent', string='Child'
    )
    qa_cases = fields.One2many(
        'quality.assurance.case', 'category', string='Cases'
    )


class qa_case_step(models.Model):
    _name = 'quality.assurance.case.step'
    _description = 'QA Case Step'

    # Columns
    sequence = fields.Integer('Sequence')
    name = fields.Char('Name', size=128)
    code = fields.Char('Code', size=32)
    result = fields.Text('Result')
    data = fields.Text('Data')
    severity = fields.Selection([
        ('mandatory', 'Mandatory'),
        ('optional', 'Optional')],
        string='Severity',
        default='mandatory'
    )
    qa_case = fields.Many2one(
        'quality.assurance.case', string="Case"
    )

'''
class project_qa_case(models.Model):
    _name = 'project.project.quality.assurance.case'
    _description = 'Project QA Case'

    # Columns
    project_qa_cases = fields.Many2one(
        'quality.assurance.case',
        'Project QA Cases'
    )


class task_qa_case(models.Model):
    _name = 'project.task.quality.assurance.case'
    _description = 'Project Task QA Case'

    # Columns
    task_qa_case_step = fields.Many2one(
        'quality.assurance.case.step',
        string="Task QA Case Step"
    )
'''


class project_task_qa_case_step_line(models.Model):
    _name = 'project.task.quality.assurance.case.step.line'
    _description = 'Project Task QA Case Step Lines'

    # Columns
    sequence = fields.Integer('Sequence')
    qa_case = fields.Many2one(
        'quality.assurance.case',
        'Case'
    )
    '''
    fields.Many2one(
        'project.project.quality.assurance.case',
        string="Project Case"
    )
    '''
    '''
    task_qa_case_step = fields.Many2one(
        'quality.assurance.case.step',
        string="Task Case Step"
    )
    '''
    '''
    fields.Many2one(
        'project.task.quality.assurance.case',
        string="Task Step"
    )
    '''
    expected_result = fields.Text('Expected Result', )
    generated_result = fields.Text('Generated Result')
    execution_status = fields.Selection(
        [
            ('na', 'N/A'),
            ('pass', 'Passed'),
            ('fail', 'Failed'),
            ('notrun', 'Not Run')
        ],
        string='Execution Status',
        default='na'
    )
    executed_by = fields.Many2one('res.users', string="Executed By")
    executed_on = fields.Date(
        string='Execution On',
        default=lambda self: self._context.get(
            'executed_on', fields.Date.context_today(self)
        )
    )
    project_id = fields.Many2one('project.project', 'Project')
    task_id = fields.Many2one('project.task', 'Task')


class project_task(models.Model):
    _inherit = 'project.task'

    # Columns
    task_qa_case_steps = fields.One2many(
        'project.task.quality.assurance.case.step.line',
        'task_id',
        'Task QA Case'
    )

    @api.multi
    def action_qa_project_task(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Project Task QA Steps',
            'res_model': 'wiz.project.task',
            'view_mode': 'form',
            'target': 'new'
        }


class project_project(models.Model):
    _inherit = 'project.project'

    # Columns
    project_qa_cases = fields.One2many(
        'project.task.quality.assurance.case.step.line',
        'project_id',
        'Project QA Case'
    )

    @api.multi
    def action_qa_project(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Project QA Steps',
            'res_model': 'wiz.project',
            'view_mode': 'form',
            'target': 'new'
        }

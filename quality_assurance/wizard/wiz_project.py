from openerp import models, fields, api


class wiz_project(models.TransientModel):
    _name = 'wiz.project'

    @api.model
    def default_get(self, fields):
        res = super(wiz_project, self).default_get(fields)
        ctx = self.env.context
        case_steps_ids = []
        if ctx and ctx.get('active_ids', False):
            active_ids = ctx.get('active_ids', False)
            project = self.env['project.project'].browse(active_ids)
            for case_step in project.project_qa_cases:
                for step in case_step.qa_case.steps:
                    case_steps_ids.append((4, step.id))
                    res.update({
                        'case_step_ids': case_steps_ids
                    })
        return res

    case_step_ids = fields.Many2many('quality.assurance.case.step', string='QA Steps')
